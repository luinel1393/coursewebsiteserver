## Courses Website

Es una aplicación para ofrecer la venta de cursos online. También cuenta con un Blog donde se muestran noticias recientes relacionadas con el mundo de la informática.

Cuenta con un panel de administración donde se podrá gestionar el contenido de la página.

Fue desarrollada siguiendo el curso de Agustín Navarro Galdon disponible en la plataforma de Udemy en:
[Web Personal MERN Full Stack: MongoDB, Express, React y Node](https://www.udemy.com/course/web-personal-mern-full-stack-mongodb-express-react-node/)

La aplicación en conjunto está conformada por un lado Cliente y un lado servidor.

### Tecnologías

Desarrollo Full Stack:

- MongoDB
- Express
- React
- Node
- Yarn para el manejo de dependencias

### Servidor

La carpeta donde está contenido el presente "README" corresponde al lado servidor de la aplicación. Al desplegar la app se pone a disposición una API REST para que sea consumida por el lado del cliente.

El puerto por defecto donde corre la aplicación es el 3977.

Instalar las depedencias e iniciar el servicio de la app.

```sh
$ cd server
$ yarn
$ yarn dev
```

#### Base de datos

Para la persistencia se utiliza MongoDB el cual ya debe estar instalado y correctamente configurado en el equipo donde se ejecuta el proyecto. En el mismo directorio del proyecto se encuentra una carpeta llamada **"cursosBD"** que contiene los archivos necesarios para restaurar la BD con sus respectivas colecciones y datos de prueba.

En caso de querer configurar los datos de conexión a la BD se puede editar el archivo **"server/config.js"**

Un usuario por defecto con el que se puede iniciar sesión en el panel de administración de la aplicación es:
Email: admin.course@yopmail.com
Contraseña: 123456
